//
//  SummonerModel.swift
//  LoLSearch
//
//  Created by admin on 09.06.15.
//  Copyright (c) 2015 de.fh-rosenheim.spp. All rights reserved.
//

import Foundation
import CoreData

@objc(SummonerModel)
class SummonerModel: NSManagedObject {

    @NSManaged var id: NSNumber
    @NSManaged var name: String
    @NSManaged var revisionDate: NSDate
    @NSManaged var profileIconId: NSNumber
    @NSManaged var summonerLevel: NSNumber
    @NSManaged var region: String
    @NSManaged var searchDate: NSDate

}
