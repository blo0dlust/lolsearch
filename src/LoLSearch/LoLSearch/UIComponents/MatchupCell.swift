import UIKit

class MatchupCell: UITableViewCell {

    @IBOutlet weak var champion1ImageView: UIImageView!
    @IBOutlet weak var champion2ImageView: UIImageView!
    @IBOutlet weak var summoner1Label: UILabel!
    @IBOutlet weak var summoner2Label: UILabel!
}
