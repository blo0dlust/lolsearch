import UIKit

class SummaryCell: UITableViewCell {

    @IBOutlet weak var championImageView: UIImageView!
    @IBOutlet weak var summonerIcon1ImageView: UIImageView!
    @IBOutlet weak var summonerIcon2ImageView: UIImageView!
    @IBOutlet weak var statsLabel: UILabel!
    @IBOutlet weak var championLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!    
}
