import UIKit

class GameCell: UITableViewCell {

    var game: GameDto? {
        didSet {
            setProperties()
        }
    }
    
    var championImage: UIImage? {
        didSet {
            setProperties()
        }
    }
    
    //
    //
    // MARK - setProperties
    private func setProperties() {
        if
            let game = game,
            let stats = game.stats,
            let champname = game.champion?.name
        {
            imageView?.image = championImage
            textLabel?.text = "\(champname) - KDA: \(stats.championsKilled)/\(stats.numDeaths)/\(stats.assists)"
            detailTextLabel?.text = stats.win ? "Victory" : "Defeat"
            detailTextLabel?.textColor = stats.win ? UIColor.greenColor() : UIColor.redColor()
        } else {
            println("----------------------------")
            println("ERROR in GameTableViewCell")
            println("game: \(game?.gameId)")
            println("stats: \(game?.stats)")
            println("----------------------------")
        }
    }
}
