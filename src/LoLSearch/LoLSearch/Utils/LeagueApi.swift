import Alamofire

class LeagueApi {
    
    private var championsLoaded = false
    private var summonerCacheById = [Int: SummonerDto]()
    private var summonerCacheByName = [String: SummonerDto]()
    private var championCache = [Int: ChampionDto]()
    private var championImageCache = [String: UIImage]()
    private var summonerIconCache = [Int: UIImage]()
    
    // MARK - Singleton
    static let sharedInstance = LeagueApi()

    // MARK - General Vars
    private let ApiKey = "7b995906-99ff-42e4-ae1c-20946feddd1e"
    private let ApiKeyParam = "?api_key=7b995906-99ff-42e4-ae1c-20946feddd1e"
    private let BaseUrl = "https://{region}.api.pvp.net/api/lol/{region}"
    private let BaseUrlStatic = "https://global.api.pvp.net/api/lol/static-data/{region}"

    private let manager = Alamofire.Manager.sharedInstance
    
    // MARK - API Urls which will be needed
    private struct Endpoints {
        static let ProfileIcon = "http://ddragon.leagueoflegends.com/cdn/5.8.1/img/profileicon/{profileIconId}.png"
        static let SummonerByName = "/v1.4/summoner/by-name/{summonerName}"
        static let SummonerById = "/v1.4/summoner/{summonerId}"
        static let MatchHistory = "/v1.3/game/by-summoner/{summonerId}/recent" // we need to call the "game" endpoint cause riot is currently working on "matchhistory"
        static let SummonerSpell = "http://ddragon.leagueoflegends.com/cdn/5.10.1/img/spell/{summonerSpell}.png"
        static let Champion = "/v1.2/champion/{championId}"
        static let AllChampions = "/v1.2/champion"
        static let ChampionImage = "http://ddragon.leagueoflegends.com/cdn/5.10.1/img/champion/{championKey}.png"
        static let MatchDetail = "/v2.2/match/{matchId}"
    }
    
    // MARK - Api Params
    private struct ApiParams {
        // General
        struct General {
            static let Region = "region"
            static let SummonerSpell = "summonerSpell"
        }
        
        // Search for Summoner
        struct Summoner {
            static let SummonerName = "summonerName"
            static let SummonerId = "summonerId"
        }
        
        // Match History
        struct MatchHistory {
            static let SummonerId = "summonerId"
        }
        
        struct Champion {
            static let ChampionId = "championId"
            static let ChampionKey = "championKey"
        }
        
        struct MatchDetails {
            static let MatchId = "matchId"
            static let IncludeTimeline = "{includeTimeline}"
        }
    }
    
    // TODO!
    private let spellKeyMap = [
        1: "SummonerBoost",
        2: "SummonerClairvoyance",
        3: "SummonerExhaust",
        4: "SummonerFlash",
        6: "SummonerHaste",
        7: "SummonerHeal",
        11: "SummonerSmite",
        12: "SummonerTeleport",
        13: "SummonerMana",
        14: "SummonerDot",
        17: "SummonerOdinGarrison",
        21: "SummonerBarrier",
        30: "SummonerPoroRecall",
        31: "SummonerPoroThrow",
        32: "SummonerSnowball"
    ]
    
    private let gameModeMap = [
        "HEXAKILL": "Hexakill",
        "NORMAL": "Normal"
    ]
    
    //
    //
    // MARK - Search for a Summonername
    func getSummoner(summonerName: String, resultHandler: (summonderModel: SummonerDto?) -> Void) {
        if let summoner = summonerCacheByName[summonerName] {
            println("Accessing SummonerCache")
            
            return resultHandler(summonderModel: summoner)
        } else {
            println("LOADING Summoner")
            let params = [ApiParams.Summoner.SummonerName: summonerName, ApiParams.General.Region: "euw"]
            let url = buildUrl(Endpoints.SummonerByName, params: params)
            
            println("Requesting: \(url)")
            
            manager.request(.GET, url, parameters: nil, encoding: .URL).responseJSON(options: NSJSONReadingOptions.AllowFragments) { (req, res, responseData, error) -> Void in
                if error != nil {
                    println(error)
                } else {
                    println("Response: \(responseData)")
                    let json = responseData as! NSDictionary
                    let summonerName = json.allKeys[0] as! String
                    
                    if let values = json[summonerName] as? [String: AnyObject] {
                        if let summoner = SummonerDto.fromJSON(values) {
                            summoner.region = params[ApiParams.General.Region]!
                            
                            self.summonerCacheById[summoner.id] = summoner
                            self.summonerCacheByName[summoner.name] = summoner
                            
                            // Call result handler
                            resultHandler(summonderModel: summoner)
                        }
                    }
                }
            }
        }
    }
    
    // Get summoner by id
    //
    //
    func getSummoners(byId summonerIds: [Int]) -> [SummonerDto] {
        let str = summonerIds.description
        let range = Range<String.Index>(start: advance(str.startIndex, 1), end: advance(str.endIndex, -1))
        let ids = str.substringWithRange(range)
        let params = [ApiParams.Summoner.SummonerId: ids.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!, ApiParams.General.Region: "euw"]
        let endpointUrl = buildUrl(Endpoints.SummonerById, params: params)
        var summoners = [SummonerDto]()
        
        if
            let url = NSURL(string: endpointUrl),
            let data = NSData(contentsOfURL: url),
            let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as? NSDictionary
        {
            println(json)
            
            for summonerId in summonerIds {
                if let summonerJson = json["\(summonerId)"] as? [String: AnyObject] {
                    if let summoner = SummonerDto.fromJSON(summonerJson) {
                        summoner.region = params[ApiParams.General.Region]!
                        
                        self.summonerCacheById[summoner.id] = summoner
                        self.summonerCacheByName[summoner.name] = summoner
                        
                        summoners.append(summoner)
                    } else {
                        println("ERROR getSummonerByID: \(json)")
                    }
                }
            }
        }
        
        return summoners
    }
    
    // Get summoner by id's async
    //
    //
    func getSummonersAsync(byId summonerIds: [Int], finishHandler: (summoners: [SummonerDto]) -> Void) {
        let qos = Int(QOS_CLASS_USER_INITIATED.value)
        
        dispatch_async(dispatch_get_global_queue(qos, 0)) {
            let fetchedSummoners = self.getSummoners(byId: summonerIds)
            
            dispatch_async(dispatch_get_main_queue()) {
                finishHandler(summoners: fetchedSummoners)
            }
        }
    }
 
    // Load summoner profile icon async
    //
    //
    func getProfileIconAsync(profileIconId: Int, finishHandler: (profileIcon: UIImage?) -> Void) {
        let qos = Int(QOS_CLASS_USER_INITIATED.value)
        dispatch_async(dispatch_get_global_queue(qos, 0)) {
            let profileIcon = self.getProfileIcon(profileIconId)
            
            dispatch_async(dispatch_get_main_queue()) {
                finishHandler(profileIcon: profileIcon)
            }
        }
    }
    
    // Load summoner proflie icon sync
    //
    // MARK - getProfileIcon
    func getProfileIcon(profileIconId: Int) -> UIImage? {
        let profileIconUrl = Endpoints.ProfileIcon.replace("{profileIconId}", withString: "\(profileIconId)")
        println("Requesting: \(profileIconUrl)")
       
        return getImage(fromUrl: profileIconUrl)
    }
    
    // Get match history from API
    //
    // MARK - getMatchHistoryForSummoner
    func getMatchHistoryForSummoner(summoner: SummonerDto, resultHandler: (matchHistory: MatchHistoryDto) -> Void) {
        let params = [ApiParams.MatchHistory.SummonerId: "\(summoner.id)", ApiParams.General.Region: summoner.region]
        let url = buildUrl(Endpoints.MatchHistory, params: params)
        
        println("Requesting: \(url)")
        
        // load from API
        manager.request(.GET, url, parameters: nil, encoding: .URL).responseJSON(options: .AllowFragments) { (req, res, responseData, error) in
            if error != nil {
                println(error)
            } else {
                println("Response: \(responseData)")
                let r = responseData as! [String: AnyObject]
                if let matchHistory = MatchHistoryDto.fromJSON(responseData as! [String: AnyObject]) {
                    
                    // load images
                    for game in matchHistory.gameModels {
                        
                        // Summoner spell icons
                        if
                            let summonerSpell1Icon = self.getSummonerSpellIcon(spellId: game.spell1),
                            let summonerSpell2Icon = self.getSummonerSpellIcon(spellId: game.spell2)
                        {
                            game.spell1Icon = summonerSpell1Icon
                            game.spell2Icon = summonerSpell2Icon
                        }
                        
                        // load champion
                        game.champion = self.getChampionDetails(byId: game.championId)
                    }
                    
                    // Call result handler
                    resultHandler(matchHistory: matchHistory)
                }
            }
        }
    }
    
    //
    //
    // MARK - getMatchDetails
    func getMatchDetails(byMatchId matchId: Int, resultHandler: (matchDetails: MatchDetailDto) -> Void) {
        let params = [ApiParams.MatchDetails.MatchId: "\(matchId)", ApiParams.General.Region: "euw"]
        let url = buildUrl(Endpoints.MatchDetail, params: params)
        
        manager.request(.GET, url, parameters: nil, encoding: .URL).responseJSON(options: .AllowFragments) { (req, res, responseData, error) in
            if error != nil {
                println(error)
            } else {
                println(responseData)
                
                if let matchDetail = MatchDetailDto.fromJSON(responseData as! [String: AnyObject]) {
                    resultHandler(matchDetails: matchDetail)
                }
            }
        }
    }
    
    // Get a single champion by id
    //
    // MARK - getChampionDetails
    func getChampionDetails(byId championId: Int) -> ChampionDto? {
        if !championsLoaded {
            getAllChampions()
            championsLoaded = true
            println("Champions loaded")
        }
        
        // get champion from cache 
        if let champion = championCache[championId] {
            return champion
        }
        
        return nil
    }
    
    // Get all Champions
    //
    // MARK - getAllChampion
    private func getAllChampions() {
        let params = [ApiParams.General.Region: "euw"]
        let url = buildStaticUrl(Endpoints.AllChampions, params: params)
        var begin = NSDate()
        if
            let url = NSURL(string: url),
            let data = NSData(contentsOfURL: url),
            let json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: nil) as? [String: AnyObject]
        {
            var end = NSDate().timeIntervalSinceDate(begin)
            println("LOADING ALL champions in \(end)")
            begin = NSDate()
            
            if let champions = json["data"] as? [String: AnyObject] {
                for (key, value) in champions {
                    if let champion = ChampionDto.fromJSON(value as! [String: AnyObject]) {
                        championCache[champion.id] = champion
                    }
                }
            }
            
            end = NSDate().timeIntervalSinceDate(begin)
            println("Storing in Cache in \(end)")
        }
    }
    
    // Get a spell icon async
    //
    //
    func getSummonerSpellIconAsync(#spellId: Int, finishHandler: (summonerSpellIcon: UIImage?) -> Void) {
        let qos = Int(QOS_CLASS_USER_INITIATED.value)
        dispatch_async(dispatch_get_global_queue(qos, 0)) {
            
            let summonerSpellIcon = self.getSummonerSpellIcon(spellId: spellId)
            
            dispatch_async(dispatch_get_main_queue()) {
                finishHandler(summonerSpellIcon: summonerSpellIcon)
            }
        }
    }
    
    // Get a Spell Icon from its id
    //
    // MARK - getSummonerSpellIcon
    func getSummonerSpellIcon(#spellId: Int) -> UIImage? {
        
        if let image = summonerIconCache[spellId] {
            return image
        } else {
            if let spellKey = spellKeyMap[spellId] {
                let urlString = Endpoints.SummonerSpell.replace("{\(ApiParams.General.SummonerSpell)}", withString: spellKey)
            
                if let image = getImage(fromUrl: urlString) {
                    summonerIconCache[spellId] = image
                    
                    return image
                }
            }
        }
        
        return nil
    }
    
    // Load champion image async
    //
    //
    func getChampionImageAsync(#championKey: String, finishHandler: (championImage: UIImage?) -> Void) {
        let qos = Int(QOS_CLASS_USER_INITIATED.value)
        dispatch_async(dispatch_get_global_queue(qos, 0)) {
            
            let championImage = self.getChampionImage(championKey: championKey)
            
            dispatch_async(dispatch_get_main_queue()) {
                finishHandler(championImage: championImage)
            }
        }
    }
    
    // Load champion image sync
    //
    //
    func getChampionImage(#championKey: String) -> UIImage? {
        let url = Endpoints.ChampionImage.replace("{\(ApiParams.Champion.ChampionKey)}", withString: championKey)
        
        return getImage(fromUrl: url)
    }
    
    // Helper method for loading image
    //
    // MARK - getImage
    private func getImage(fromUrl imageUrl: String) -> UIImage? {
        if
            let url = NSURL(string: imageUrl),
            let data = NSData(contentsOfURL: url),
            let image = UIImage(data: data)
        {
            return image
        }
        
        return nil
    }
    
    //
    //
    // MARK - buildStaticUrl
    private func buildStaticUrl(endpoint: String, params: [String: String]?) -> String {
        return buildUrl(BaseUrlStatic, endpoint: endpoint, params: params)
    }
    
    //
    //
    // MARK - buildUrl
    private func buildUrl(endpoint: String, params: [String:String]?) -> String {
        return buildUrl(BaseUrl, endpoint: endpoint, params: params)
    }
    
    //
    //
    // MARK - buildUrl - withBaseUrl
    private func buildUrl(withBaseUrl: String, endpoint: String, params: [String: String]?) -> String {
        var url = withBaseUrl + endpoint + ApiKeyParam
        
        if let params = params {
            // replace params
            for (key, value) in params {
                url = url.replace("{\(key)}", withString: value)
            }
        }
        
        return url
    }
   


}

extension String
{
    func replace(target: String, withString: String) -> String
    {
        return self.stringByReplacingOccurrencesOfString(target, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
}