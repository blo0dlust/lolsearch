
import UIKit

class SearchResultViewController: UITableViewController {

    var searchedSummonerName: String? {
        didSet {
            searchForSummoner()
        }
    }
    
    private var summoner: SummonerDto?

    //
    //
    // MARK - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide searchbar on startup
        tableView.setContentOffset(CGPointMake(0, tableView.tableHeaderView!.frame.size.height), animated: true)
    }
    
    //
    //
    // MARK - searchForSummoner
    func searchForSummoner() {
        // Search for Summoner
        LeagueApi.sharedInstance.getSummoner(searchedSummonerName!) { (SummonerDto: SummonerDto?) in
            self.summoner = SummonerDto
            self.reloadTableView()
        }
    }
    
    //
    //
    // MARK - Reload data
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
    //
    //
    // MARK - NumberOfSectionsInTableView
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //
    //
    // MARK - TableView - NumberOfRowsInSection
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    //
    //
    // MARK - TableView - CellForRowAtIndexPath
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var tableCell = tableView.dequeueReusableCellWithIdentifier(Identifier.Cell, forIndexPath: indexPath) as? UITableViewCell
        
        if tableCell == nil {
            tableCell = UITableViewCell()
        }
        
        if let currentSummoner = summoner {
        
            tableCell?.textLabel?.text = currentSummoner.name
            tableCell?.detailTextLabel?.text = "Level: \(currentSummoner.summonerLevel)"
            
            LeagueApi.sharedInstance.getProfileIconAsync(currentSummoner.profileIconId) { (profileIcon: UIImage?) in
                tableCell?.imageView?.image = profileIcon
            }
        }
        
        
        return tableCell!
    }
    
    //
    //
    // MARK - prepareForSegue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let matchHistoryVC = segue.destinationViewController as? MatchHistoryViewController {
            matchHistoryVC.summoner = summoner!
        }
    }
    
    private struct Identifier {
        static let Cell = "searchSummonerNameCell"
    }
    
}
