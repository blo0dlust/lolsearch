import UIKit
import CoreData

class SearchTableViewController: UITableViewController, UISearchBarDelegate {

    @IBOutlet weak var summonerSearchBar: UISearchBar!
    
    private var searchDelayTimer: NSTimer?
    private var recentSearchResults = [SummonerDto]()
    private var searchedSummoners = [SummonerDto]()
    
    var isSearchActive = false {
        didSet {
            println("Changed isSearchActive: \(isSearchActive)")
        }
    }

    var dataSource : [SummonerDto]? {
        get {
            return isSearchActive ? searchedSummoners : recentSearchResults
        }
    }
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext!
    
    //
    //
    // MARK - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let color = UIColor(red: 200 / 255.0, green: 180 / 255.0, blue: 40 / 255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = color
        
        summonerSearchBar.delegate = self
        
        // hide search bar by default
        tableView.setContentOffset(CGPointMake(0, 44), animated: false)
        
        // load data
        getRecentSearchResults()
    }
    
    //
    //
    // MARK - getRecentSearchResults
    func getRecentSearchResults() {
        let fetchRequest = NSFetchRequest(entityName: "SummonerModel")
        let sort = NSSortDescriptor(key: "searchDate", ascending: false)
        fetchRequest.sortDescriptors = [sort]
        
        if let fetchResult = managedObjectContext.executeFetchRequest(fetchRequest, error: nil) as? [SummonerModel] {
            for summoner in fetchResult {
                let sum = SummonerDto.fromModel(summoner)
                recentSearchResults.append(sum)
            }
            
            tableView.reloadData()
        }
    }
    
    //
    //
    //
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //
    //
    //
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return isSearchActive ? "Suchergebnis" : "Letzen Suchresultate"
    }
   
    //
    //
    //
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let sectionHeader = tableView.headerViewForSection(section) {
            sectionHeader.tintColor = UIColor.orangeColor()
            sectionHeader.backgroundColor = UIColor.orangeColor()
            
            return sectionHeader
        }
        
        return tableView.headerViewForSection(section)
    }
    
    //
    //
    // MARK - tableView numbersOfRowsInSection
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let summoners = dataSource {
            return summoners.count
        }
        
        return 0
    }
    
    //
    //
    // MARK - tableView cellForRowAtIndexPath
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var tableCell = tableView.dequeueReusableCellWithIdentifier(Constants.CellIdentifier) as? UITableViewCell
        
        if tableCell == nil {
            tableCell = UITableViewCell()
        }
        
        if
            let ds = dataSource,
            let cell = tableCell
        {
            let summoner = ds[indexPath.row]
            cell.textLabel?.text = summoner.name
            cell.detailTextLabel?.text = "Level: \(summoner.summonerLevel)"
            
            LeagueApi.sharedInstance.getProfileIconAsync(summoner.profileIconId) { (profileIcon: UIImage?) in
                cell.imageView?.image = profileIcon
                cell.setNeedsLayout()
            }
        }
        
        return tableCell!
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        println("searchBarTextDidBeginEditing")
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        println("searchBarTextDidEndEditing")
        isSearchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        println("searchBarCancelButtonClicked")
        isSearchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        println("searchBarSearchButtonClicked")
        isSearchActive = true;
        
        doSearch(searchBar.text)
    }
    
    // Reset search
    //
    // MARK - searchBar textDidChanged
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if count(searchText) == 0 {
            isSearchActive = false
            tableView.reloadData()
        }
        
        println(searchText)
    }
    
    // Search the API for a summonername
    //
    // MARK - doSearch
    func doSearch(summonerName: String) {
        if count(summonerName) > 0 {
            LeagueApi.sharedInstance.getSummoner(summonerName) { (summoner: SummonerDto?) in
                if let sum = summoner {
                    self.searchedSummoners.removeAll(keepCapacity: true)
                    self.searchedSummoners.append(sum)
                    self.addSummonerToRecentSearchResults(sum)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    //
    //
    // MARK - addSummonerToRecentSearchResults
    private func addSummonerToRecentSearchResults(summoner: SummonerDto) {
        // look for this summoner
        let fetchRequest = NSFetchRequest(entityName: "SummonerModel")
        let predicate = NSPredicate(format: "id = %i", summoner.id)
        fetchRequest.predicate = predicate
        let foundSummoner = managedObjectContext.executeFetchRequest(fetchRequest, error: nil) as! [SummonerModel]
        
        // We have this summoner in list, so update searchDate and put it to front
        if foundSummoner.count > 0 {
            for sum in foundSummoner {
                sum.searchDate = NSDate()
            }
            
            // move the summoner out of the recentsearchresult
            var index = -1
            
            for (i, sum) in enumerate(recentSearchResults) {
                if sum.id == summoner.id {
                    index = i
                    break
                }
            }
            
            if index > -1 {
                let old = recentSearchResults.removeAtIndex(index)
                recentSearchResults.insert(old, atIndex: 0)
            }
            
            managedObjectContext.save(nil)
        } else { // else add it to dict/db
            let newSummoner = NSEntityDescription.insertNewObjectForEntityForName(AppDelegate.Models.SummonerModel, inManagedObjectContext: managedObjectContext) as! SummonerModel
            summoner.mapTo(summonerModel: newSummoner)
            newSummoner.searchDate = NSDate()
            recentSearchResults.insert(summoner, atIndex: 0)
            
            managedObjectContext.save(nil)
        }
    }
    

    
    // Go to MatchHistoryViewController on click on a cell
    //
    // MARK - prepareForSegue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let matchHistoryVC = segue.destinationViewController as? MatchHistoryViewController {
            
            // Get the path of selected row
            if let path = tableView.indexPathForSelectedRow(),
               let ds = dataSource {
                    println("Row index \(path.row)")
                    matchHistoryVC.summoner = ds[path.row]
            }
        }
    }
    
    // MARK - Keys
    private struct Constants {
        static let RecentSearchResults = "LoLSearch.SearchTableViewController.RecentSearchResults"
        static let DoDelayedSearchSelector = "doDelayedSearch"
        static let SearchDelay = NSTimeInterval(1.5)
        static let CellIdentifier = "summonerCell"
        static let ShowMatchHistorySegue = "showMatchHistorySegue"
    }
}
