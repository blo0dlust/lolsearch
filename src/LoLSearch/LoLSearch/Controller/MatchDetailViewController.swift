import UIKit


class MatchDetailViewController: UITableViewController {

    @IBOutlet weak var champView: UIView!
    @IBOutlet weak var champImage: UIImageView!
    
    var game: GameDto?
    var selectedSummoner: SummonerDto?
    var matchDetails: MatchDetailDto?
    var gameStats: [(title: String, value: AnyObject?)]?
    var team1 = [ParticipantDto]()
    var team2 = [ParticipantDto]()
    var summoners = [String: SummonerDto]()
    let sections = ["Summary", "Matchups", "Game Stats"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        
        getMatchDetails()
    }
    
    private func getMatchDetails() {
        if let game = game {
            LeagueApi.sharedInstance.getMatchDetails(byMatchId: game.gameId) { (matchDetails: MatchDetailDto) in
                self.matchDetails = matchDetails
                
                // Split teams
                for participant in self.matchDetails!.participants {
                    if participant.teamId == Constants.team1 {
                        self.team1.append(participant)
                    } else if participant.teamId == Constants.team2 {
                        self.team2.append(participant)
                    }
                }
                
                self.displayData()
            }
        }
    }
    
    
    
    // Set data
    //
    //
    private func displayData() {
        println(matchDetails)
        
        if let stats = game?.stats {
            // set the game stats data
            gameStats = [
                // General stats
                ("Kills", stats.championsKilled),
                ("Deaths", stats.numDeaths),
                ("Assists", stats.assists),
                ("Largest Multikill", stats.largestMultiKill),
                ("Gold earned", stats.goldEarned),
                
                // Minions killed
                ("Minions killed", stats.minionsKilled),
                ("Neutral minions killed", stats.neutralMinionsKilled),
                ("Neutral Monsters killed enemy Jungle", stats.neutralMinionsKilledEnemyJungle),
                
                // Damage to champions
                ("Damage dealt to Champions", stats.totalDamageDealtToChampions),
                ("Physical damage dealt Champions", stats.physicalDamageDealtToChampions),
                ("Magic damage dealt Champions", stats.magicDamageDealtToChampions),
                ("True damage dealt Champions", stats.trueDamageDealtToChampions),
                
                // Damage total
                ("Total Damage", stats.totalDamageDealt),
                ("Physical damage dealt", stats.physicalDamageDealtPlayer),
                ("Magic damage dealt", stats.magicDamageDealtPlayer),
                ("True damage dealt", stats.trueDamageDealtPlayer),
                
                // Damage taken
                ("Damage Taken", stats.totalDamageTaken),
                ("Physical damage taken", stats.physicalDamageTaken),
                ("Magic damage taken", stats.magicDamageTaken),
                ("True damage taken", stats.trueDamageTaken),
                
                // else
                ("Time played", stats.timePlayed),
                ("Total heal", stats.totalHeal),
                ("Total units healed", stats.totalUnitsHealed),
                ("Wards killed", stats.wardKilled),
                ("Wards placed", stats.wardPlaced),
                ("Time CC dealt", stats.totalTimeCrowdControlDealt)
            ]
        }
        
        tableView.reloadData()
    }
    
    // NumbersOfSections - 3 (Summary/Matchups/Gamestats)
    //
    //
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    // Get the title for a section
    //
    //
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    // Calculate number of rows
    //
    //
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        
        switch section {
        case 0:
            rowCount = 1
            
        case 1:
            println("Seciton1 (Matchups): \(matchDetails?.participants.count)")
    
            if let matchDetails = matchDetails {
              rowCount = matchDetails.participants.count / 2
            }
            
        case 2:
            println("Section2 (Gamestats): \(matchDetails?.participants[0].stats?.rawData.count)")
            
            if let stats = gameStats {
                rowCount = stats.count
            }
            
        default:
            rowCount = 0
        }
        
        return rowCount
    }
    
    // Create cells depending on section
    //
    //
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            return createSummaryCell(tableView, cellForRowAtIndexPath: indexPath)
            
        case 1:
            return createMatchupCell(tableView, cellForRowAtIndexPath: indexPath)
            
        case 2:
            return createGamestatsCell(tableView, cellForRowAtIndexPath: indexPath)
            
        default:
            return UITableViewCell()
        }
    }
    
    // Create the summary cell
    //
    //
    private func createSummaryCell(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(Identifier.summaryCell) as? SummaryCell
        
        if cell == nil {
            cell = SummaryCell()
        }
        
        let summaryCell = cell!

        // load images async
        if
            let championKey = game?.champion?.key,
            let spell1 = game?.spell1,
            let spell2 = game?.spell2
        {
        
            let qos = Int(QOS_CLASS_USER_INITIATED.value)
            dispatch_async(dispatch_get_global_queue(qos, 0)) {
                
                let championImage = LeagueApi.sharedInstance.getChampionImage(championKey: championKey)
                let spell1Icon = LeagueApi.sharedInstance.getSummonerSpellIcon(spellId: spell1)
                let spell2Icon = LeagueApi.sharedInstance.getSummonerSpellIcon(spellId: spell2)
                
                dispatch_async(dispatch_get_main_queue()) {
                    // Check if its still the same cell
                    if championKey == self.game?.champion?.key {
                        summaryCell.championImageView.image = championImage
                        summaryCell.summonerIcon1ImageView.image = spell1Icon
                        summaryCell.summonerIcon2ImageView.image = spell2Icon
                    }
                }
            }
        }
        
        if let stats = game?.stats {
            summaryCell.statsLabel.text = "KDA: \(stats.championsKilled) / \(stats.numDeaths) / \(stats.assists)"
            summaryCell.resultLabel.text = stats.win ? "Victory" : "Defeat"
            summaryCell.resultLabel.textColor = stats.win ? UIColor.greenColor() : UIColor.redColor()
            
            if let champion = game?.champion {
                summaryCell.championLabel.text = "\(champion.name)"
            }
        }
        
        return summaryCell
    }
    
    // Create a cell for matchup
    //
    //
    private func createMatchupCell(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(Identifier.matchupCell) as? MatchupCell
        
        if cell == nil {
            cell = MatchupCell()
        }
        
        let matchupCell = cell!
        
        if
            let team1FellowPlayers = game?.team1FellowPlayersByChampionId,
            let team2FellowPlayers = game?.team2FellowPlayersByChampionId,
            let participants = matchDetails?.participants,
            let selfChamp = game?.champion
        {
            let team1Participant = team1[indexPath.row]
            let team2Participant = team2[indexPath.row]
            
            let team1Champion = LeagueApi.sharedInstance.getChampionDetails(byId: team1Participant.championId)
            let team2Champion = LeagueApi.sharedInstance.getChampionDetails(byId: team2Participant.championId)
            
            let team1SummonerId = team1FellowPlayers[team1Participant.championId]?.summonerId ?? game?.summonerId
            let team2SummonerId = team2FellowPlayers[team2Participant.championId]?.summonerId ?? game?.summonerId
            
            LeagueApi.sharedInstance.getSummonersAsync(byId: [team1SummonerId!, team2SummonerId!]) { (summoners: [SummonerDto]) in
            
                if summoners.count == 2 {
                    let team1Summoner = summoners[0]
                    let team2Summoner = summoners[1]
                    
                    self.summoners[team1Summoner.name] = team1Summoner
                    self.summoners[team2Summoner.name] = team2Summoner
                   
                    
                    // Load image for champion
                    if
                        let team1Champ = team1Champion?.key,
                        let team2Champ = team2Champion?.key
                    {
                        LeagueApi.sharedInstance.getChampionImageAsync(championKey: team1Champ) { (championImage: UIImage?) in
                            matchupCell.champion1ImageView.image = championImage
                        }
                        
                        LeagueApi.sharedInstance.getChampionImageAsync(championKey: team2Champ) { (championImage: UIImage?) in
                            matchupCell.champion2ImageView.image = championImage
                        }
                    }
                    
                    matchupCell.summoner1Label.text = team1Summoner.name
                    matchupCell.summoner2Label.text = team2Summoner.name
                    
                    matchupCell.summoner1Label.userInteractionEnabled = true
                    matchupCell.summoner2Label.userInteractionEnabled = true
                    
                    var tapGesture = UITapGestureRecognizer(target: self, action: Selector("summonerLabelClicked:"))
                    tapGesture.numberOfTapsRequired = 1
                    
                    matchupCell.summoner1Label.addGestureRecognizer(tapGesture)
                    
                    tapGesture = UITapGestureRecognizer(target: self, action: Selector("summonerLabelClicked:"))
                    tapGesture.numberOfTapsRequired = 1
                    
                    matchupCell.summoner2Label.addGestureRecognizer(tapGesture)
                }
            }
        }
        
        return matchupCell
    }
    
    // On summoner label click switch to his match history
    //
    //
    func summonerLabelClicked(sender: UITapGestureRecognizer) {
        if let label = sender.view as? UILabel {
            if let summoner = summoners[label.text!] {
                
                if summoner.id != game?.summonerId {
                    selectedSummoner = summoner
                
                    performSegueWithIdentifier(Identifier.showHistorySegue, sender: self)
                }
            }
        }
    }
    
    // Create a cell for gamestats
    //
    //
    private func createGamestatsCell(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(Identifier.gamestatsCell) as? GamestatsCell
        
        if cell == nil {
            cell = GamestatsCell()
        }
        
        let gamestatsCell = cell!
        
        if let stats = gameStats {
            let cellWidth = (tableView.bounds.width - 16) / 2
            
            if gamestatsCell.titleLabel == nil {
                gamestatsCell.titleLabel = UILabel(frame: CGRectMake(8, 0, cellWidth, gamestatsCell.bounds.height))
                gamestatsCell.addSubview(gamestatsCell.titleLabel!)
            }
            
            if gamestatsCell.valueLabel == nil {
                gamestatsCell.valueLabel = UILabel(frame: CGRectMake(cellWidth + 8, 0, cellWidth, gamestatsCell.bounds.height))
                gamestatsCell.valueLabel?.textAlignment = .Right
                gamestatsCell.addSubview(gamestatsCell.valueLabel!)
            }
            
            let formatter = NSNumberFormatter()
            formatter.formatterBehavior = NSNumberFormatterBehavior.Behavior10_4
            formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
            
            let stat = stats[indexPath.row]
            
            if let value = stat.value as? Int {
                gamestatsCell.titleLabel?.text = stat.title
                gamestatsCell.valueLabel?.text = formatter.stringFromNumber(value)
            }
        }
        
        return gamestatsCell
    }

    // Go back to Summonerlist
    //
    //
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let summoner = selectedSummoner {
            if let matchHistoryVC = segue.destinationViewController as? MatchHistoryViewController {
                matchHistoryVC.summoner = summoner
            }
        }
    }
    
    private struct Constants {
        static let team1 = 100
        static let team2 = 200
    }
    
    private struct Identifier {
        static let summaryCell = "summaryCell"
        static let matchupCell = "matchupCell"
        static let gamestatsCell = "gamestatsCell"
        static let showHistorySegue = "showMatchHistorySegueFromDetails"
    }
}
