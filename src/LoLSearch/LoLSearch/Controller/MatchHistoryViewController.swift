import UIKit

class MatchHistoryViewController : UITableViewController {
    
    var summoner: SummonerDto? {
        didSet {
            getMatchHistory()
        }
    }
    
    var matchHistory: MatchHistoryDto?
    
    //
    //
    // MARK - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Get match history for summoner
    //
    // MARK - getMatchHistory
    func getMatchHistory() {
        LeagueApi.sharedInstance.getMatchHistoryForSummoner(summoner!) { (matchHistory: MatchHistoryDto) in
            self.matchHistory = matchHistory
            self.tableView.reloadData()
        }
    }
    
    //
    //
    // MARK - NumberOfSectionsInTableView
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //
    //
    // MARK - TableView - NumberOfRowsInSection
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = matchHistory?.gameModels.count {
            return count
        }
        
        return 0
    }
    
    //
    //
    // MARK - TableView - CellForRowAtIndexPath
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var tableCell = tableView.dequeueReusableCellWithIdentifier(Identifier.Cell, forIndexPath: indexPath) as? GameCell
        
        if tableCell == nil {
            tableCell = GameCell()
        }
        
        if let currentGame = matchHistory?.gameModels[indexPath.row] {
            // get champion            
            tableCell?.game = currentGame
            
            if let championKey = currentGame.champion?.key {
                LeagueApi.sharedInstance.getChampionImageAsync(championKey: championKey) { (championImage: UIImage?) in
                    tableCell?.championImage = championImage
                    tableCell?.setNeedsLayout()
                }
            }
        } else {
            println("----------------------------")
            println("ERROR at row \(indexPath.row)")
            println("----------------------------")
        }
        
        
        return tableCell!
    }
    
    //
    //
    // MARK - prepareForSegue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if
            let matchDetailVC = segue.destinationViewController as? MatchDetailViewController,
            let gameCell = sender as? GameCell
        {
            matchDetailVC.game = gameCell.game
        }
    }
    
    
    private struct Identifier {
        static let Cell = "gameCell"
        static let MatchDetailSegue = "showMatchDetailSegue"
    }
}
