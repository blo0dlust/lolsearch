class GameStatsDto {
    
    private(set) var rawData: [String: AnyObject] = [String: AnyObject]()

    private(set) var level: Int = 0
    private(set) var championsKilled: Int = 0
    private(set) var numDeaths: Int = 0
    private(set) var assists: Int = 0
    private(set) var goldEarned: Int = 0
    private(set) var minionsKilled: Int = 0
    private(set) var team: Int = 0
    private(set) var win: Bool = false
    private(set) var totalDamageDealt: Int = 0
    private(set) var totalDamageTaken: Int = 0
    private(set) var neutralMinionsKilled: Int = 0
    private(set) var largestMultiKill: Int = 0
    private(set) var physicalDamageDealtPlayer: Int = 0
    private(set) var magicDamageDealtPlayer: Int = 0
    private(set) var physicalDamageTaken: Int = 0
    private(set) var magicDamageTaken: Int = 0
    private(set) var timePlayed: Int = 0
    private(set) var totalHeal: Int = 0
    private(set) var totalUnitsHealed: Int = 0
    private(set) var item0: Int = 0
    private(set) var item1: Int = 0
    private(set) var item2: Int = 0
    private(set) var item3: Int = 0
    private(set) var item4: Int = 0
    private(set) var item5: Int = 0
    private(set) var item6: Int = 0
    private(set) var magicDamageDealtToChampions: Int = 0
    private(set) var physicalDamageDealtToChampions: Int = 0
    private(set) var totalDamageDealtToChampions: Int = 0
    private(set) var trueDamageDealtPlayer: Int = 0
    private(set) var trueDamageDealtToChampions: Int = 0
    private(set) var trueDamageTaken: Int = 0
    private(set) var wardKilled: Int = 0
    private(set) var wardPlaced: Int = 0
    private(set) var neutralMinionsKilledEnemyJungle: Int = 0
    private(set) var totalTimeCrowdControlDealt: Int = 0
    private(set) var playerRole: Int = 0
    private(set) var playerPosition: Int = 0
    
    //
    //
    // MARK - fromJSON
    static func fromJSON(json: [String: AnyObject]) -> GameStatsDto? {
        
        let gamestats = GameStatsDto()
        gamestats.rawData = json
        
        // set tats otherwise default value
        gamestats.level = json[Keys.level] as? Int ?? 0
        gamestats.championsKilled = json[Keys.championsKilled] as? Int ?? 0
        gamestats.numDeaths = json[Keys.numDeaths] as? Int ?? 0
        gamestats.assists = json[Keys.assists] as? Int ?? 0
        gamestats.goldEarned = json[Keys.goldEarned] as? Int ?? 0
        gamestats.minionsKilled = json[Keys.minionsKilled] as? Int ?? 0
        gamestats.team = json[Keys.team] as? Int ?? 0
        gamestats.win = json[Keys.win] as? Bool ?? false
        gamestats.totalDamageDealt = json[Keys.totalDamageDealt] as? Int ?? 0
        gamestats.totalDamageTaken = json[Keys.totalDamageTaken] as? Int ?? 0
        gamestats.neutralMinionsKilled = json[Keys.neutralMinionsKilled] as? Int ?? 0
        gamestats.largestMultiKill = json[Keys.largestMultiKill] as? Int ?? 0
        gamestats.physicalDamageDealtPlayer = json[Keys.physicalDamageDealtPlayer] as? Int ?? 0
        gamestats.magicDamageDealtPlayer = json[Keys.magicDamageDealtPlayer] as? Int ?? 0
        gamestats.physicalDamageTaken = json[Keys.physicalDamageTaken] as? Int ?? 0
        gamestats.magicDamageTaken = json[Keys.magicDamageTaken] as? Int ?? 0
        gamestats.timePlayed = json[Keys.timePlayed] as? Int ?? 0
        gamestats.totalHeal = json[Keys.totalHeal] as? Int ?? 0
        gamestats.totalUnitsHealed = json[Keys.totalUnitsHealed] as? Int ?? 0
        gamestats.magicDamageDealtToChampions = json[Keys.magicDamageDealtToChampions] as? Int ?? 0
        gamestats.physicalDamageDealtToChampions = json[Keys.physicalDamageDealtToChampions] as? Int ?? 0
        gamestats.totalDamageDealtToChampions = json[Keys.totalDamageDealtToChampions] as? Int ?? 0
        gamestats.trueDamageDealtPlayer = json[Keys.trueDamageDealtPlayer] as? Int ?? 0
        gamestats.trueDamageDealtToChampions = json[Keys.trueDamageDealtToChampions] as? Int ?? 0
        gamestats.trueDamageTaken = json[Keys.trueDamageTaken] as? Int ?? 0
        gamestats.wardKilled = json[Keys.wardKilled] as? Int ?? 0
        gamestats.wardPlaced = json[Keys.wardPlaced] as? Int ?? 0
        gamestats.neutralMinionsKilledEnemyJungle = json[Keys.neutralMinionsKilledEnemyJungle] as? Int ?? 0
        gamestats.totalTimeCrowdControlDealt = json[Keys.totalTimeCrowdControlDealt] as? Int ?? 0
        gamestats.playerRole = json[Keys.playerRole] as? Int ?? 0
        gamestats.playerPosition = json[Keys.playerPosition] as? Int ?? 0
        gamestats.item0 = json[Keys.item0] as? Int ?? 0
        gamestats.item1 = json[Keys.item1] as? Int ?? 0
        gamestats.item2 = json[Keys.item2] as? Int ?? 0
        gamestats.item3 = json[Keys.item3] as? Int ?? 0
        gamestats.item4 = json[Keys.item4] as? Int ?? 0
        gamestats.item5 = json[Keys.item5] as? Int ?? 0
        gamestats.item6 = json[Keys.item6] as? Int ?? 0
        
        return gamestats
        
    }
    
    //
    //
    // MARK - Keys
    private struct Keys {
        static let level = "level"
        static let championsKilled = "championsKilled"
        static let numDeaths = "numDeaths"
        static let assists = "assists"
        static let goldEarned = "goldEarned"
        static let minionsKilled = "minionsKilled"
        static let team = "team"
        static let win = "win"
        static let totalDamageDealt = "totalDamageDealt"
        static let totalDamageTaken = "totalDamageTaken"
        static let neutralMinionsKilled = "neutralMinionsKilled"
        static let largestMultiKill = "largestMultiKill"
        static let physicalDamageDealtPlayer = "physicalDamageDealtPlayer"
        static let magicDamageDealtPlayer = "magicDamageDealtPlayer"
        static let physicalDamageTaken = "physicalDamageTaken"
        static let magicDamageTaken = "magicDamageTaken"
        static let timePlayed = "timePlayed"
        static let totalHeal = "totalHeal"
        static let totalUnitsHealed = "totalUnitsHealed"
        static let item0 = "item0"
        static let item1 = "item1"
        static let item2 = "item2"
        static let item3 = "item3"
        static let item4 = "item4"
        static let item5 = "item5"
        static let item6 = "item6"
        static let magicDamageDealtToChampions = "magicDamageDealtToChampions"
        static let physicalDamageDealtToChampions = "physicalDamageDealtToChampions"
        static let totalDamageDealtToChampions = "totalDamageDealtToChampions"
        static let trueDamageDealtPlayer = "trueDamageDealtPlayer"
        static let trueDamageDealtToChampions = "trueDamageDealtToChampions"
        static let trueDamageTaken = "trueDamageTaken"
        static let wardKilled = "wardKilled"
        static let wardPlaced = "wardPlaced"
        static let neutralMinionsKilledEnemyJungle = "neutralMinionsKilledEnemyJungle"
        static let totalTimeCrowdControlDealt = "totalTimeCrowdControlDealt"
        static let playerRole = "playerRole"
        static let playerPosition = "playerPosition"
    }
}