class ParticipantDto {
    private(set) var teamId = 0
    private(set) var spell1Id = 0
    private(set) var spell2Id = 0
    private(set) var participantId = 0
    private(set) var championId = 0
    var stats: ChampionStatsDto?
 
    //
    //
    // MARK - fromJSON
    static func fromJSON(json: [String: AnyObject]) -> ParticipantDto? {
        
        if
            let teamId = json[Keys.teamId] as? Int,
            let spell1Id = json[Keys.spell1Id] as? Int,
            let spell2Id = json[Keys.spell2Id] as? Int,
            let participantId = json[Keys.participantId] as? Int,
            let championId = json[Keys.championId] as? Int
        {
            var participant = ParticipantDto()
            participant.teamId = teamId
            participant.spell1Id = spell1Id
            participant.spell2Id = spell2Id
            participant.participantId = participantId
            participant.championId = championId
                        
            // get stats for participant
            if let stats = json[Keys.stats] as? [String: AnyObject] {
                participant.stats = ChampionStatsDto.fromJSON(stats)
            }
            
            return participant
        }
        
        return nil
    }
    
    private struct Keys {
        static let teamId = "teamId"
        static let spell1Id = "spell1Id"
        static let spell2Id = "spell2Id"
        static let participantId = "participantId"
        static let championId = "championId"
        static let stats = "stats"
    }
}