class MatchDetailDto {
    private(set) var matchId = 0
    private(set) var matchCreation = 0
    private(set) var matchDuration = 0
    private(set) var mapId = 0
    var participants = [ParticipantDto]()
    
    // Create MatchDetail Object form json
    //
    // MARK - fromJSON
    static func fromJSON(json: [String: AnyObject]) -> MatchDetailDto? {
        
        if
            let matchId = json[Keys.matchId] as? Int,
            let matchCreation = json[Keys.matchCreation] as? Int,
            let matchDuration = json[Keys.matchDuration] as? Int,
            let mapId = json[Keys.mapId] as? Int
        {
            var matchDetail = MatchDetailDto()
            matchDetail.matchId = matchId
            matchDetail.matchCreation = matchCreation
            matchDetail.matchDuration = matchDuration
            matchDetail.mapId = mapId
            
            // get participants
            if let participants = json[Keys.participants] as? [[String: AnyObject]] {
                for participant in participants {
                    if let participant = ParticipantDto.fromJSON(participant) {
                        matchDetail.participants.append(participant)
                    }
                }
            }
            
            return matchDetail
        }
        
        return nil
    }
    
    private struct Keys {
        static let matchId = "matchId"
        static let matchCreation = "matchCreation"
        static let matchDuration = "matchDuration"
        static let mapId = "mapId"
        static let participants = "participants"
    }
}