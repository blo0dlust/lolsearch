class FellowplayerDto {
    private(set) var summonerId: Int = 0
    private(set) var teamId: Int = 0
    private(set) var championId: Int = 0
   
    // Create Fellowplayer from JSON
    //
    // MARK - fromJSON
    static func fromJSON(json: [String: AnyObject]) -> FellowplayerDto? {
        
        if
            let summonerId = json[Keys.SummonerId] as? Int,
            let teamId = json[Keys.TeamId] as? Int,
            let championId = json[Keys.ChampionId] as? Int
        {
            let fellowPlayer = FellowplayerDto()

            fellowPlayer.summonerId = summonerId
            fellowPlayer.teamId = teamId
            fellowPlayer.championId = championId
            
            return fellowPlayer
        }
        
        return nil
    }
    
    private struct Keys {
        static let SummonerId = "summonerId"
        static let TeamId = "teamId"
        static let ChampionId = "championId"
    }
}