//
//
//
//
import UIKit

class SummonerDto {
    
    /*private(set)*/ var id: Int = 0
    private(set) var name: String = ""
    private(set) var profileIconId: Int = 0
    private(set) var revisionDate: NSDate = NSDate()
    private(set) var summonerLevel: Int = 0
    var region: String = ""
    
    //
    //
    // MARK - toDictionary
    func toJSON() -> String {
        let values: [String: AnyObject] = [
            Keys.Id: id,
            Keys.Name: name,
            Keys.ProfileIconId: profileIconId,
            Keys.RevisionDate: revisionDate,
            Keys.SummonerLevel: summonerLevel,
        ]
        
        let json = NSJSONSerialization.dataWithJSONObject(values, options: NSJSONWritingOptions(0), error: nil)
        let jsonText = NSString(data: json!, encoding: NSASCIIStringEncoding)
        
        return jsonText! as! String
    }
    
    //
    //
    // MARK - Create Object form json
    static func fromJSON(json: NSDictionary) -> SummonerDto? {
        if let
            id = json[Keys.Id] as? Int,
            name = json[Keys.Name] as? String,
            profileIconId = json[Keys.ProfileIconId] as? Int,
            revisionDate = json[Keys.RevisionDate] as? Int,
            summonerLevel = json[Keys.SummonerLevel] as? Int
        {
            var summoner = SummonerDto()
            summoner.id = id
            summoner.name = name
            summoner.profileIconId = profileIconId
            summoner.revisionDate = NSDate(timeIntervalSince1970: NSTimeInterval(revisionDate))
            summoner.summonerLevel = summonerLevel
            
            return summoner
        }
        
        return nil
    }
    
    //
    //
    // MARK - fromModel
    static func fromModel(model: SummonerModel) -> SummonerDto {
        let summoner = SummonerDto()
        
        summoner.id = model.id as! Int
        summoner.name = model.name
        summoner.profileIconId = model.profileIconId as! Int
        summoner.revisionDate = model.revisionDate
        summoner.summonerLevel = model.summonerLevel as! Int
        summoner.region = model.region
        
        return summoner
    }
    
    //
    //
    // MARK - mapTo summonerModel
    func mapTo(#summonerModel: SummonerModel) {
        summonerModel.id = self.id
        summonerModel.name = self.name
        summonerModel.profileIconId = self.profileIconId
        summonerModel.revisionDate = self.revisionDate
        summonerModel.summonerLevel = self.summonerLevel
        summonerModel.region = self.region
    }
    
    //
    //
    // MARK - Keys for json
    private struct Keys {
        static let Id = "id"
        static let Name = "name"
        static let ProfileIconId = "profileIconId"
        static let RevisionDate = "revisionDate"
        static let SummonerLevel = "summonerLevel"
        static let SummonerIcon = "summonerIcon"
        static let Region = "region"
    }
}