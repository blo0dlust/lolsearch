class ChampionStatsDto {
    
    private(set) var rawData: [String: AnyObject] = [String: AnyObject]()
    private(set) var winner: Bool = false
    private(set) var champLevel: Int = 0
    private(set) var item0: Int = 0
    private(set) var item1: Int = 0
    private(set) var item2: Int = 0
    private(set) var item3: Int = 0
    private(set) var item4: Int = 0
    private(set) var item5: Int = 0
    private(set) var item6: Int = 0
    private(set) var kills: Int = 0
    private(set) var doubleKills: Int = 0
    private(set) var tripleKills: Int = 0
    private(set) var quadrakills: Int = 0
    private(set) var pentaKills: Int = 0
    private(set) var unrealKills: Int = 0
    private(set) var largestKillingSpree: Int = 0
    private(set) var deaths: Int = 0
    private(set) var assists: Int = 0
    private(set) var totalDamageDealt: Int = 0
    private(set) var totalDamageDealtToChampions: Int = 0
    private(set) var totalDamageTaken: Int = 0
    private(set) var largestCriticalStrike: Int = 0
    private(set) var totalHeal: Int = 0
    private(set) var minionsKilled: Int = 0
    private(set) var neutralMinionsKilled: Int = 0
    private(set) var neutralMinionsKilledTeamJungle: Int = 0
    private(set) var neutralMinionsKilledEnemyJungle: Int = 0
    private(set) var goldEarned: Int = 0
    private(set) var goldSpent: Int = 0
    private(set) var combatPlayerScore: Int = 0
    private(set) var objectivePlayerScore: Int = 0
    private(set) var totalPlayerScore: Int = 0
    private(set) var totalScoreRank: Int = 0
    private(set) var magicDamageDealtToChampions: Int = 0
    private(set) var physicalDamageDealtToChampions: Int = 0
    private(set) var trueDamageDealtToChampions: Int = 0
    private(set) var visionWardsBoughtInGame: Int = 0
    private(set) var sightWardsBoughtInGame: Int = 0
    private(set) var magicDamageDealt: Int = 0
    private(set) var physicalDamageDealt: Int = 0
    private(set) var trueDamageDealt: Int = 0
    private(set) var magicDamageTaken: Int = 0
    private(set) var physicalDamageTaken: Int = 0
    private(set) var trueDamageTaken: Int = 0
    private(set) var firstBloodKill: Bool = false
    private(set) var firstBloodAssist: Bool = false
    private(set) var firstTowerKill: Bool = false
    private(set) var firstTowerAssist: Bool = false
    private(set) var firstInhibitorKill: Bool = false
    private(set) var firstInhibitorAssist: Bool = false
    private(set) var inhibitorKills: Int = 0
    private(set) var towerKills: Int = 0
    private(set) var wardsPlaced: Int = 0
    private(set) var wardsKilled: Int = 0
    private(set) var largestMultiKill: Int = 0
    private(set) var killingSprees: Int = 0
    private(set) var totalUnitsHealed: Int = 0
    private(set) var totalTimeCrowdControlDealt: Int = 0
    
    //
    //
    // MARK - fromJSON
    static func fromJSON(json: [String: AnyObject]) -> ChampionStatsDto? {
        
        if
            let winner = json[Keys.winner] as? Bool,
            let champLevel = json[Keys.champLevel] as? Int,
            let item0 = json[Keys.item0] as? Int,
            let item1 = json[Keys.item1] as? Int,
            let item2 = json[Keys.item2] as? Int,
            let item3 = json[Keys.item3] as? Int,
            let item4 = json[Keys.item4] as? Int,
            let item5 = json[Keys.item5] as? Int,
            let item6 = json[Keys.item6] as? Int,
            let kills = json[Keys.kills] as? Int,
            let doubleKills = json[Keys.doubleKills] as? Int,
            let tripleKills = json[Keys.tripleKills] as? Int,
            let quadraKills = json[Keys.quadrakills] as? Int,
            let pentaKills = json[Keys.pentaKills] as? Int,
            let unrealKills = json[Keys.unrealKills] as? Int,
            let largestKillingSpree = json[Keys.largestKillingSpree] as? Int,
            let deaths = json[Keys.deaths] as? Int,
            let assists = json[Keys.assists] as? Int,
            let totalDamageDealt = json[Keys.totalDamageDealt] as? Int,
            let totalDamageDealtToChampions = json[Keys.totalDamageDealtToChampions]  as? Int,
            let totalDamageTaken = json[Keys.totalDamageTaken] as? Int,
            let largestCriticalStrike = json[Keys.largestCriticalStrike]  as? Int,
            let totalHeal = json[Keys.totalHeal] as? Int,
            let minionsKilled = json[Keys.minionsKilled] as? Int,
            let neutralMinionsKilled = json[Keys.neutralMinionsKilled] as? Int,
            let neutralMinionsKilledTeamJungle = json[Keys.neutralMinionsKilledTeamJungle] as? Int,
            let neutralMinionsKilledEnemyJungle = json[Keys.neutralMinionsKilledEnemyJungle] as? Int,
            let goldEarned = json[Keys.goldEarned] as? Int,
            let goldSpent = json[Keys.goldSpent] as? Int,
            let combatPlayerScore = json[Keys.combatPlayerScore] as? Int,
            let objectivePlayerScore = json[Keys.objectivePlayerScore] as? Int,
            let totalPlayerScore = json[Keys.totalPlayerScore] as? Int,
            let totalScoreRank = json[Keys.totalScoreRank] as? Int,
            let magicDamageDealtToChampions = json[Keys.magicDamageDealtToChampions] as? Int,
            let physicalDamageDealtToChampions = json[Keys.physicalDamageDealtToChampions] as? Int,
            let trueDamageDealtToChampions = json[Keys.trueDamageDealtToChampions] as? Int,
            let visionWardsBoughtInGame = json[Keys.visionWardsBoughtInGame] as? Int,
            let sightWardsBoughtInGame = json[Keys.sightWardsBoughtInGame] as? Int,
            let magicDamageDealt = json[Keys.magicDamageDealt] as? Int,
            let physicalDamageDealt = json[Keys.physicalDamageDealt] as? Int,
            let trueDamageDealt = json[Keys.trueDamageDealt] as? Int,
            let magicDamageTaken = json[Keys.magicDamageTaken] as? Int,
            let physicalDamageTaken = json[Keys.physicalDamageTaken] as? Int,
            let trueDamageTaken = json[Keys.trueDamageTaken] as? Int,
            let firstBloodKill = json[Keys.firstBloodKill] as? Bool,
            let firstBloodAssist = json[Keys.firstBloodAssist] as? Bool,
            let firstTowerKill = json[Keys.firstTowerKill] as? Bool,
            let firstTowerAssist = json[Keys.firstTowerAssist] as? Bool,
            let firstInhibitorKill = json[Keys.firstInhibitorKill] as? Bool,
            let firstInhibitorAssist = json[Keys.firstInhibitorAssist] as? Bool,
            let inhibitorKills = json[Keys.inhibitorKills] as? Int,
            let towerKills = json[Keys.towerKills] as? Int,
            let wardsPlaced = json[Keys.wardsPlaced] as? Int,
            let wardsKilled = json[Keys.wardsKilled] as? Int,
            let largestMultiKill = json[Keys.largestMultiKill] as? Int,
            let killingSprees = json[Keys.killingSprees] as? Int,
            let totalUnitsHealed = json[Keys.totalUnitsHealed] as? Int,
            let totalTimeCrowdControlDealt = json[Keys.totalTimeCrowdControlDealt] as? Int
        {
            var stats = ChampionStatsDto()
            stats.rawData = json
            stats.winner = winner
            stats.champLevel = champLevel
            stats.item0 = item0
            stats.item1 = item1
            stats.item2 = item2
            stats.item3 = item3
            stats.item4 = item4
            stats.item5 = item5
            stats.item6 = item6
            stats.kills = kills
            stats.doubleKills = doubleKills
            stats.tripleKills = tripleKills
            stats.quadrakills = quadraKills
            stats.pentaKills = pentaKills
            stats.unrealKills = unrealKills
            stats.largestKillingSpree = largestKillingSpree
            stats.deaths = deaths
            stats.assists = assists
            stats.totalDamageDealt = totalDamageDealt
            stats.totalDamageDealtToChampions = totalDamageDealtToChampions
            stats.totalDamageTaken = totalDamageTaken
            stats.largestCriticalStrike = largestCriticalStrike
            stats.totalHeal = totalHeal
            stats.minionsKilled = minionsKilled
            stats.neutralMinionsKilled = neutralMinionsKilled
            stats.neutralMinionsKilledTeamJungle = neutralMinionsKilledTeamJungle
            stats.neutralMinionsKilledEnemyJungle = neutralMinionsKilledEnemyJungle
            stats.goldEarned = goldEarned
            stats.goldSpent = goldSpent
            stats.combatPlayerScore = combatPlayerScore
            stats.objectivePlayerScore = objectivePlayerScore
            stats.totalPlayerScore = totalPlayerScore
            stats.totalScoreRank = totalScoreRank
            stats.magicDamageDealtToChampions = magicDamageDealtToChampions
            stats.physicalDamageDealtToChampions = physicalDamageDealtToChampions
            stats.trueDamageDealtToChampions = trueDamageDealtToChampions
            stats.visionWardsBoughtInGame = visionWardsBoughtInGame
            stats.sightWardsBoughtInGame = sightWardsBoughtInGame
            stats.magicDamageDealt = magicDamageDealt
            stats.physicalDamageDealt = physicalDamageDealt
            stats.trueDamageDealt = trueDamageDealt
            stats.magicDamageTaken = magicDamageTaken
            stats.physicalDamageTaken = physicalDamageTaken
            stats.trueDamageTaken = trueDamageTaken
            stats.firstBloodKill = firstBloodKill
            stats.firstBloodAssist = firstBloodAssist
            stats.firstTowerKill = firstTowerKill
            stats.firstTowerAssist = firstTowerAssist
            stats.firstInhibitorKill = firstInhibitorKill
            stats.firstInhibitorAssist = firstInhibitorAssist
            stats.towerKills = towerKills
            stats.inhibitorKills = inhibitorKills
            stats.wardsPlaced = wardsPlaced
            stats.wardsKilled = wardsKilled
            stats.largestMultiKill = largestMultiKill
            stats.killingSprees = killingSprees
            stats.totalUnitsHealed = totalUnitsHealed
            stats.totalTimeCrowdControlDealt = totalTimeCrowdControlDealt
            
            return stats
        }
        
        return nil
    }

    let strings = [
        Keys.winner: "Winner",
        Keys.champLevel: "Champion Level",
        Keys.item0: "Item #1",
        Keys.item1: "Item #2",
        Keys.item2: "Item #3",
        Keys.item3: "Item #4",
        Keys.item4: "Item #5",
        Keys.item5: "Item #6",
        Keys.item6: "Item #7",
        Keys.kills: "Kills",
        Keys.doubleKills: "Doublekills",
        Keys.tripleKills: "Triplekills",
        Keys.quadrakills: "Quadrakills",
        Keys.pentaKills: "Pentakills",
        Keys.unrealKills: "Unrealkills",
        Keys.largestKillingSpree: "Largest killingspree",
    ]
    
    private struct Keys {
        static let winner = "winner"
        static let champLevel = "champLevel"
        static let item0 = "item0"
        static let item1 = "item1"
        static let item2 = "item2"
        static let item3 = "item3"
        static let item4 = "item4"
        static let item5 = "item5"
        static let item6 = "item6"
        static let kills = "kills"
        static let doubleKills = "doubleKills"
        static let tripleKills = "tripleKills"
        static let quadrakills = "quadraKills"
        static let pentaKills = "pentaKills"
        static let unrealKills = "unrealKills"
        static let largestKillingSpree = "largestKillingSpree"
        static let deaths = "deaths"
        static let assists = "assists"
        static let totalDamageDealt = "totalDamageDealt"
        static let totalDamageDealtToChampions = "totalDamageDealtToChampions"
        static let totalDamageTaken = "totalDamageTaken"
        static let largestCriticalStrike = "largestCriticalStrike"
        static let totalHeal = "totalHeal"
        static let minionsKilled = "minionsKilled"
        static let neutralMinionsKilled = "neutralMinionsKilled"
        static let neutralMinionsKilledTeamJungle = "neutralMinionsKilledTeamJungle"
        static let neutralMinionsKilledEnemyJungle = "neutralMinionsKilledEnemyJungle"
        static let goldEarned = "goldEarned"
        static let goldSpent = "goldSpent"
        static let combatPlayerScore = "combatPlayerScore"
        static let objectivePlayerScore = "objectivePlayerScore"
        static let totalPlayerScore = "totalPlayerScore"
        static let totalScoreRank = "totalScoreRank"
        static let magicDamageDealtToChampions = "magicDamageDealtToChampions"
        static let physicalDamageDealtToChampions = "physicalDamageDealtToChampions"
        static let trueDamageDealtToChampions = "trueDamageDealtToChampions"
        static let visionWardsBoughtInGame = "visionWardsBoughtInGame"
        static let sightWardsBoughtInGame = "sightWardsBoughtInGame"
        static let magicDamageDealt = "magicDamageDealt"
        static let physicalDamageDealt = "physicalDamageDealt"
        static let trueDamageDealt = "trueDamageDealt"
        static let magicDamageTaken = "magicDamageTaken"
        static let physicalDamageTaken = "physicalDamageTaken"
        static let trueDamageTaken = "trueDamageTaken"
        static let firstBloodKill = "firstBloodKill"
        static let firstBloodAssist = "firstBloodAssist"
        static let firstTowerKill = "firstTowerKill"
        static let firstTowerAssist = "firstTowerAssist"
        static let firstInhibitorKill = "firstInhibitorKill"
        static let firstInhibitorAssist = "firstInhibitorAssist"
        static let inhibitorKills = "inhibitorKills"
        static let towerKills = "towerKills"
        static let wardsPlaced = "wardsPlaced"
        static let wardsKilled = "wardsKilled"
        static let largestMultiKill = "largestMultiKill"
        static let killingSprees = "killingSprees"
        static let totalUnitsHealed = "totalUnitsHealed"
        static let totalTimeCrowdControlDealt = "totalTimeCrowdControlDealt"
    }
}