import UIKit

class GameDto {
    var summonerId: Int = 0
    private(set) var gameId: Int = 0
    private(set) var gameMode: String = ""
    private(set) var subType: String = ""
    private(set) var teamId: Int = 0
    private(set) var championId: Int = 0
    private(set) var spell1: Int = 0
    private(set) var spell2: Int = 0
    private(set) var level: Int = 0
    private(set) var stats: GameStatsDto?
    private(set) var fellowPlayersBySummonerId = [Int: FellowplayerDto]()
    private(set) var team1FellowPlayersByChampionId = [Int: FellowplayerDto]()
    private(set) var team2FellowPlayersByChampionId = [Int: FellowplayerDto]()
    var champion: ChampionDto?
    
    var spell1Icon: UIImage?
    var spell2Icon: UIImage?
    
    static func fromJSON(json: [String: AnyObject]) -> GameDto? {
        
        // Get the values out of the dictionary
        if
            let gameId = json[Keys.GameId] as? Int,
            let gameMode = json[Keys.GameMode] as? String,
            let subType = json[Keys.SubType] as? String,
            let teamId = json[Keys.TeamId] as? Int,
            let championId = json[Keys.ChampionId] as? Int,
            let spell1 = json[Keys.Spell1] as? Int,
            let spell2 = json[Keys.Spell2] as? Int,
            let level = json[Keys.Level] as? Int
        {
            let game = GameDto()
            game.gameId = gameId
            game.gameMode = gameMode
            game.subType = subType
            game.teamId = teamId
            game.championId = championId
            game.spell1 = spell1
            game.spell2 = spell2
            game.level = level
        
            
            // stats
            if let stats = json[Keys.Stats] as? [String: AnyObject] {
                 game.stats = GameStatsDto.fromJSON(stats)
            }
            
            if game.stats == nil {
                println("ERROR in if let stats = json (GameID: \(game.gameId)")
            }
            
            
            // fellowplayers
            if let players = json[Keys.FellowPlayers] as? [[String: AnyObject]] {
                for player in players {
                    if let fellowPlayer = FellowplayerDto.fromJSON(player) {
                        
                        game.fellowPlayersBySummonerId[fellowPlayer.summonerId] = fellowPlayer
                        
                        if fellowPlayer.teamId == 100 {
                            game.team1FellowPlayersByChampionId[fellowPlayer.championId] = fellowPlayer
                        } else if fellowPlayer.teamId == 200 {
                            game.team2FellowPlayersByChampionId[fellowPlayer.championId] = fellowPlayer
                        }
                    }
                }
            }
            
            return game
        }
        
        return nil
    }
    
    //
    //
    // MARK - Keys
    private struct Keys {
        static let GameId = "gameId"
        static let GameMode = "gameMode"
        static let SubType = "subType"
        static let TeamId = "teamId"
        static let ChampionId = "championId"
        static let Spell1 = "spell1"
        static let Spell2 = "spell2"
        static let Level = "level"
        
        static let Stats = "stats"
        static let FellowPlayers = "fellowPlayers"
    }
}
