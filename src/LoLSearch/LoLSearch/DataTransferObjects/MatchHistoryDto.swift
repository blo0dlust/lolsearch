import UIKit

class MatchHistoryDto {
    
    private(set) var gameModels: [GameDto] = [GameDto]()
    
    //
    //
    // MARK - fromJSON
    static func fromJSON(json: NSDictionary) -> MatchHistoryDto? {
        
        if
            let summonerId = json[Keys.summonerId] as? Int,
            let games = json[Keys.games] as? [[String: AnyObject]]
        {
            println("Found \(games.count) games")
            
            let matchHistory = MatchHistoryDto()
                        
            // Get the game objects
            for game in games {
                if let gameDto = GameDto.fromJSON(game) {
                    gameDto.summonerId = summonerId
                    matchHistory.gameModels.append(gameDto)
                }
            }
            
            return matchHistory
        }
        
        return nil
    }
    
    //
    //
    // MARK - Keys
    private struct Keys {
        static let summonerId = "summonerId"
        static let games = "games"
    }
}