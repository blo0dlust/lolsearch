import UIKit

class ChampionDto {
    private(set) var id: Int = 0
    private(set) var name: String = ""
    private(set) var key: String = ""
    
    // Create Object from JSON
    //
    // MARK - fromJSON
    static func fromJSON(json: [String: AnyObject?]) -> ChampionDto? {
    
        if
            let id = json[Keys.id] as? Int,
            let name = json[Keys.name] as? String,
            let key = json[Keys.key] as? String
        {
            var champ = ChampionDto()
            champ.id = id
            champ.name = name
            champ.key = key
            
            return champ
        }
        
        return nil
    }
    
    private struct Keys {
        static let id = "id"
        static let name = "name"
        static let key = "key"
    }
}
